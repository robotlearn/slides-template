# LaTeX beamer template for RobotLearn

![](template_preview.png)

Feel free to contribute by creating a merge request or by
[opening an issue](https://gitlab.inria.fr/robotlearn/slides-template/-/issues/new).
